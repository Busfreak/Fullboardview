<?php

namespace Kanboard\Plugin\Fullboardview;

use Kanboard\Core\Plugin\Base;

class Plugin extends Base
{
    public function initialize()
    {

	$this->template->hook->attach('template:dashboard:sidebar', 'fullboardview:dashboard/dashboardsidebar');
	$this->template->hook->attach('template:project:dropdown', 'fullboardview:project/dropdown');

    }

    public function getPluginName()
    {
        return 'Fullboardview';
    }
    public function getPluginAuthor()
    {
        return 'TTJ';
    }
    public function getPluginVersion()
    {
        return '0.0.1';
    }
    public function getPluginDescription()
    {
        return 'View all boards on the same page';
    }
    public function getPluginHomepage()
    {
        return '';
    }
}
