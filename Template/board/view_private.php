<style>

header {
	display: none;
}

header .wakeup {
	display: inherit;
}

#kill {
	display: inherit;
}

.project-header {
	display: none;
}
/*
.stopit ~ header {
	display: none;
}

.stopit ~ project-header {
	display: none;
}*/
</style>

<?php 
if($countCSS == "1"){ ?>

<?= $this->render('fullboardview:header', array(
	'projectDrop' => $projectDrop,
	'title' => "All boards"
))
 ?>
<?php } ?>

<?php
 ?>

<section id="main">
    <?= $this->projectHeader->render($project, 'BoardViewController', 'show', true) ?>

	<br>

    <?php /* $this->render('Test:board/dropdown2', array(
	'project' => $project
	)) */?>

	<br>

    <?= $this->render('Fullboardview:board/table_container', array(
		'boardColExp' => $boardColExp,
	'title' => $title,
        'project' => $project,
        'swimlanes' => $swimlanes,
        'board_private_refresh_interval' => $board_private_refresh_interval,
        'board_highlight_period' => $board_highlight_period,
    )) ?>

<?php  ?>

<?php
/* view_public
<section id="main" class="public-board">
    <?= $this->projectHeader->render($project, 'BoardViewController', 'show', true) ?>

   <?= $this->render('board/table_container', array(
            'project' => $project,
            'swimlanes' => $swimlanes,
            'board_private_refresh_interval' => $board_private_refresh_interval,
            'board_highlight_period' => $board_highlight_period,
            'not_editable' => true,
    )) ?>
*/ ?>
</section>


