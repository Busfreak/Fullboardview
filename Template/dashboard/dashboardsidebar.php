<li>
<?= $this->url->link(t('All boards collapsed'), 'FullboardviewController', 'allboardcollapsed', array('plugin' => 'fullboardview', 'boardColExp' => 'col')) ?>
</li>
<li>
<?= $this->url->link(t('All boards expanded'), 'FullboardviewController', 'allboardexpanded', array('plugin' => 'fullboardview', 'boardColExp' => 'exp')) ?>
</li>
