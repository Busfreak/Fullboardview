    <li>
       <i class="fa fa-object-group fa-fw"></i>&nbsp;
        <?= $this->url->link(t('All boards collapsed'), 'FullboardviewController', 'allboardcollapsed', array('plugin' => 'fullboardview', 'project_id' => $project['id'], 'boardColExp' => 'col')) ?>
    </li>
    <li>
       <i class="fa fa-object-group fa-fw"></i>&nbsp;
        <?= $this->url->link(t('All boards expanded'), 'FullboardviewController', 'allboardexpanded', array('plugin' => 'fullboardview', 'project_id' => $project['id'], 'boardColExp' => 'exp')) ?>
    </li>

